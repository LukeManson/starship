## Starship config
Custom configuration for starship terminal prompt

### Installation
* clone this repository
```sh
git clone git@gitlab.com:LukeManson/starship.git ~/.config/starship
```
* install starship from https://starship.rs
* add the following to the end of .bashrc or .zshrc and uncomment the necessary line
```sh
export STARSHIP_CONFIG=~/.config/starship/starship.toml
# eval "$(starship init bash)" # uncomment for bash
# eval "$(starship init zsh)" # uncomment for zsh
```
